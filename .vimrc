set nocompatible
filetype on
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#rc()
Bundle 'gmarik/vundle'

Bundle 'tpope/vim-surround'
Bundle 'gcmt/breeze.vim'
Bundle 'ctrlpvim/ctrlp.vim'
Bundle 'tomtom/tcomment_vim'
Bundle 'bling/vim-airline'
Bundle 'vim-airline/vim-airline-themes'
Bundle 'airblade/vim-gitgutter'

" Monokai
Bundle 'lsdr/monokai'
Bundle 'jpalardy/vim-slime'

" C formatting
Plugin 'Chiel92/vim-autoformat'
" Erlang formatting
set runtimepath^=~/.vim/bundle/vim-erlang-runtime/
" Tab completion
Plugin 'ervandew/supertab'

" Color Themes
Bundle 'flazz/vim-colorschemes'
Bundle 'robertmeta/nofrils'
Bundle 'baskerville/bubblegum'

""""""""
if has('autocmd')
  filetype plugin indent on
endif
if has('syntax') && !exists('g:syntax_on')
  syntax enable
endif

" Use :help 'option' to see the documentation for the given option.
set autoindent
set backspace=indent,eol,start
set complete-=i
set showmatch
set showmode

set nrformats-=octal
set shiftround

set ttimeout

set incsearch

set laststatus=2
set ruler
set showcmd
set wildmenu

set autoread

set encoding=utf-8
set listchars=tab:▒░,trail:▓
set list

inoremap <C-U> <C-G>u<C-U>

set number
set hlsearch
set ignorecase
set smartcase

" do not history when leave buffer
set hidden

set nobackup
set nowritebackup
set noswapfile
set fileformats=unix,dos,mac

set completeopt=menuone,longest,preview

"
" Plugins config
"

" CtrlP
set wildignore+=*/.git/*,*/.hg/*,*/.svn/*

" vim-airline
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1

"
" Basic shortcuts definitions
"  most in visual mode / selection (v or ⇧ v)
"

" indent / deindent after selecting the text with (⇧ v), (.) to repeat.
vnoremap <Tab> >
vnoremap <S-Tab> <
" comment / decomment & normal comment behavior
vmap <C-m> gc
" Disable tComment to escape some entities
let g:tcomment#replacements_xml={}
" Tabs
let g:airline_theme='papercolor' " badwolf'
let g:airline#extensions#tabline#enabled = 1

" this machine's config
if filereadable(expand("~/.vimrc.local"))
  source ~/.vimrc.local
endif

""""""""""""""""""""""""""""""""""""""
" My personal config options follow: "
""""""""""""""""""""""""""""""""""""""
colorscheme sol-term
let g:nofrils_strbackgrounds=1
let g:ctrlp_root_markers = ['dev']
let g:ctrlp_working_path_mode = 'car'
let g:ctrlp_max_files = 0

if has("gui_running")
    set guifont=Monaco:h14
    "Source\ Code\ Pro\ for\ Powerline\ Medium\ 10
    set guioptions=aiA
    set columns=86 lines=64
endif

set undofile
set undodir=$HOME/.vim/undo
set undolevels=100000000000" How many undos
set undoreload=100000000000" n lines to save for undo

set splitright

autocmd BufRead,BufNewFile *.asm setlocal filetype=nasm
autocmd BufRead,BufNewFile *.nasm setlocal filetype=nasm

" set_Co=256 " My terminal supports 256colours, but lies. *sigh*

ab csp let @/ = ""
ab Q q
ab W w

fu! FormatStuff()
	%s/\s\+$//ge
	%s/	/	/g
endfunction
fu! Runline()
    :.w !bash
endfunction

let mapleader = 'f'
map  <leader>f :w<Enter>
map  <leader>s :let @/ = ""<Enter>
map  <leader>q :q<Enter>
map  <leader>n :tabnew<Enter>
" map  <leader>k ddkkp
" map  <leader>j ddp
" map  <leader>n :diffthis<Enter>
" map  <leader>m :diffoff<Enter>
map  <Leader>g :GitGutterToggle<Enter>
map  <leader>d :call Runline()<Enter>
map  <leader>v "+y<Enter>
map  <leader>r <C-t>
map  <leader>p :CtrlP ~/dev/<Enter>

set shiftwidth=4
set tabstop=4
set softtabstop=4
set expandtab
set sw=4

" neovim stuff
tnoremap <Esc> <C-\><C-n>
nmap <C-P> :bp<CR>
nmap <C-N> :bn<CR>

